#include "bmp.h"
#include <stdbool.h>
#include <stdint.h>

#include "image.h"
#include "util.h"

uint16_t biBitCount = 24;
uint16_t bfType = 19778;
uint32_t bOffBits = 54;
uint32_t biSize = 40;

#pragma pack(push, 1)
struct /*__attribute__((packed))*/ header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static uint8_t get_padding(uint64_t width) {
    return width % 4;
}

static enum read_status check_header(struct header const header) {

    if (header.biBitCount != biBitCount) return READ_INVALID_BITS;
    if (header.bfType != bfType) return READ_INVALID_SIGNATURE;
    if (header.bOffBits != bOffBits) return READ_INVALID_OFFSET;
    if (header.biSize != biSize) return READ_INVALID_SIZE;

    return READ_OK;
}

enum read_status from_bmp(FILE* const fp, struct image* image) {
    struct header header;
    if(!fread(&header, sizeof(struct header), 1, fp)) return READ_INVALID_HEADER;

    if (check_header(header) != READ_OK)
        return check_header(header);

    image->width = header.biWidth;
    image->height = header.biHeight;
    image->data = malloc(sizeof(struct pixel) * image->height * image->width);

    const uint8_t padding = get_padding(image->width);
    size_t row_count;
    for (size_t i=0; i < image->height; i++) {
        row_count = fread(&(image->data[i * image->width]), sizeof(struct pixel), image->width, fp);
        int check_fseek = fseek(fp, padding, SEEK_CUR);
        if (image->width != row_count || check_fseek != 0) return READ_INVALID_BITS;
    }

    return READ_OK;
}


enum write_status to_bmp(FILE* fp, struct image const* image) {
    const uint32_t biSizeImage = image->width * image->height * sizeof(struct pixel) + image->height * get_padding(image->width);
    const uint32_t biFileSize = sizeof(struct header) + biSizeImage;

    struct header header = {
            bfType, biFileSize, 0, sizeof(struct header), biSize,
            image->width, image->height, 1, biBitCount, 0, biSizeImage,
            0, 0, 0, 0
    };

    if(!fwrite(&header, sizeof(struct header), 1, fp))
        return WRITE_ERROR_HEADER;

    const uint8_t padding = get_padding(image->width);
    size_t row_count;
    const uint64_t paddingByte = 0;
    for (size_t i=0; i < image->height; i++) {
        row_count = fwrite(&(image->data[i * image->width]), sizeof(struct pixel), image->width, fp);
        fwrite(&paddingByte, 1, padding, fp); // write padding bytes

        if (image->width != row_count)
            return WRITE_ERROR_DATA;
    }
    return WRITE_OK;
}
