#include "image.h"

#include <stdint.h>
#include <stdlib.h>

struct image image_create(uint64_t width, uint64_t height) {
    struct image created = {
            .width = width,
            .height = height,
            .data = malloc(width*height*sizeof(struct pixel))
    };
    return created;
}

void image_free(struct image* image){
    if (image){
        free(image->data);
        image->data = NULL;
        image->height = 0;
        image->width = 0;
    }
}
