#include "rotate.h"

#include "image.h"

size_t get_pixel_address(size_t width, size_t i, size_t j){
    return i * width + j;
}

struct image rotate(const struct image source) {
    struct image rotated = image_create(source.height, source.width);
    for (size_t i = 0; i < rotated.height; i++) {
        const size_t si = i;
        for (size_t j = 0; j < rotated.width; j++) {
            const size_t sj = rotated.width - j - 1;
            rotated.data[ get_pixel_address( rotated.width, i, j ) ] = source.data[ get_pixel_address( source.width, sj, si ) ];
        }
    }
    return rotated;
}
