#ifndef STRUCT_IMAGE
#define STRUCT_IMAGE

#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create(uint64_t width, uint64_t height);
void image_free(struct image* image);
#endif
