#ifndef FORMAT_BMP
#define FORMAT_BMP

#include "image.h"
#include "util.h"
#include <stdbool.h>
#include <stdint.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_OFFSET,
    READ_INVALID_SIZE
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR_HEADER,
    WRITE_ERROR_DATA
};

enum read_status from_bmp(FILE* fp, struct image* image);
enum write_status to_bmp(FILE* fp, struct image const* image);
#endif
